/// <reference path="Promise.ts" />
import PromiseInTs = Test.Promise;

let p1: PromiseInTs = new PromiseInTs((resolve, reject) => {
    resolve('promise resolved');
})
    .then((val) => {
        console.log('then1: ' + val);
        return 123;
    })
    .then((val) => {
        console.log('then2: ' + val);
        return new PromiseInTs((resolve, reject) => {
            setTimeout(() => {
                resolve('then2 after 2000ms');
            }, 2000);
        });
    })
    .then((val) => {
        console.log('then3: ' + val);
    });
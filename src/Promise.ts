namespace Test {
    enum PromiseStatus {
        PENDING = -1,
        RESOLVED = 0,
        REJECTED = 1
    };

    // Data object
    class DeferredData {
        private _actionFunc: Function[];
        constructor(onResolved?: Function, onRejected?: Function) {
            this._actionFunc = [];
            this._actionFunc[PromiseStatus.RESOLVED] = onResolved;
            this._actionFunc[PromiseStatus.REJECTED] = onRejected;
        }

        public execute(type: PromiseStatus, param): any {
            let result: any;
            if (this._actionFunc[type]) {
                result = this._actionFunc[type](param);
            }
            return result;
        }
    }

    const log = (message: string) => {
        console.log(`[Promise] ${message}`);
    }
    export class Promise {
        public static arr: Promise[] = [];
        public id: number;

        private _status: PromiseStatus;

        // to restore the data during go through the chain of then or catch
        private _currentData: any;
        // to store the function that need to be executed later
        private _deferredDataArr: DeferredData[];

        constructor(setupFunc: (callbackWhenResolve: (result?: any) => void, callbackWhenReject: (reason?: any) => void) => void) {
            Promise.arr.push(this);
            this.id = Promise.arr.length;

            this._status = PromiseStatus.PENDING;
            this._deferredDataArr = [];

            //need to bind because _resolve and _reject will call by other object
            //for example: window when use setTimeout
            setupFunc(this._resolve.bind(this), this._reject.bind(this));
        }

        private _updateDataByDeferredData(deferredData: DeferredData): Promise {
            let tmpData: any;
            let promiseForNextChain: Promise;
            tmpData = deferredData.execute(this._status, this._currentData);
            if (tmpData instanceof Promise) {
                log('return a new Promise object, prepare to transfer left deferred to it');
                promiseForNextChain = tmpData;
            } else {
                this._currentData = tmpData;
                promiseForNextChain = this;
            }
            return promiseForNextChain;
        }

        private _executeDeferredList() {
            let promiseForNextChain: Promise;
            let tmpDeferredData: DeferredData;

            while (tmpDeferredData = this._deferredDataArr.shift()) {
                if (promiseForNextChain) {
                    // transfer deferredData to next new Promise object
                    promiseForNextChain.addDeferredData(tmpDeferredData);
                } else {
                    // only deal with the action that match the complete status
                    promiseForNextChain = this._updateDataByDeferredData(tmpDeferredData);
                }
            }
        }

        //help function for resolve, reject
        private _complete(status: PromiseStatus, data?: any) {
            log(this.id + " complete !");
            switch (this._status) {
                case PromiseStatus.PENDING:
                    this._status = status;
                    this._currentData = data;
                    this._executeDeferredList();
                    break;
                default:
                    throw new Error('Promise status can only be changed at first time');
                    break;
            }
        }

        private _resolve(result?: any) {
            this._complete(PromiseStatus.RESOLVED, result);
        }

        private _reject(reason?: any) {
            this._complete(PromiseStatus.REJECTED, reason);
        }

        public then(onResolved?: Function, onRejected?: Function): Promise {
            let tmpDeferredData: DeferredData = new DeferredData(onResolved, onRejected);
            let promiseForNextChain: Promise;

            switch (this._status) {
                case PromiseStatus.PENDING:
                    this.addDeferredData(tmpDeferredData);
                    break;
                default:
                    promiseForNextChain = this._updateDataByDeferredData(tmpDeferredData);
                    break;
            }
            return promiseForNextChain;
        }

        //help function
        public catch(onRejected: (reason?: any) => any): Promise {
            return this.then(undefined, onRejected);
        }

        public addDeferredData(data: DeferredData) {
            this._deferredDataArr.push(data);
        }

        // static method
        public static all(promises: Promise[]): Promise {
            return new Promise((resolve, reject) => {
                let dataArr = [];
                let targetCounter: number = promises.length;
                let currentCounter: number = 0;

                promises.forEach((promise: Promise, index: number) => {
                    promise.then((returnData) => {
                        dataArr[index] = returnData;
                        currentCounter++;
                        if (currentCounter === targetCounter) {
                            resolve(dataArr);
                        }
                    }).catch(reject);
                });
            });
        }

        public static race(promises: Promise[]): Promise {
            let maxFailTimes = promises.length;
            let failCounter = 0;
            let resultPromise: Promise;

            resultPromise = new Promise((resolve, reject) => {
                promises.forEach((promise: Promise) => {
                    promise.then((result) => {
                        if (resultPromise._status === PromiseStatus.PENDING) {
                            resolve(result);
                        }
                    }).catch(() => {
                        failCounter++;
                        if (failCounter === maxFailTimes) {
                            reject('All promise rejected');
                        }
                    });
                })
            });

            return resultPromise;
        }
    }
}
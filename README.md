# install
```
npm install
```

# build
```
gulp
```

# Note
## about tsconfig
when using namespace:
1. don't use module system
2. use outFile option
> Generate one javascript and one definition file.

## about chaining
> Since `then` and `catch` have the same functionality, I just talk about the situation of `then` and `resolved`.

the parameter that passed to `then` is a function (this function will be mentioned as `actionFunc` later).

### from the first Promise of the chain:
When the state of Promise is resolved, I can execute the `actionFunc` directly.
When the state of Promise is pending, `actionFunc` need to be excuted when the async process is complete. So I need a DeferredData object to record it.

### from the first return value of `actionFunc`:
The `actionFunc` may return two type of data: Promise and non Promise.

non Promise: Promise pass this data to next `actionFunc` as parameter.
I can execute `actionFunc` one by one until the end of chain or another Promise.

Promise: the next `actionFunc` need to wait until this Promise is resolved.
I need a DeferredData object to record it (again).
At the same time, I need to add the rest DeferredData objects to the new Promise Object.
When this new Promise object is resolved, it will executed the DeferredData object in its own array.

## when async(or sync) process complete
> Since `resolve` and `reject` have the same process, I just talk about the situation of `resolve`.

When the process is resolved, the `_resolve` function inside Promise object will be called.
From now on, Promise object start to deal with the `actionFunc` in its `_deferredDataArr`.
I take out one deferredData object in the FIFO order (as the same order as the then method executed).
I use one variable to pass the value: this._currentData.
If the return value of deferredData.execute is another Promise, I transfer the rest deferredData object to this new Promise.
Otherwise, I restore the result in this._currentData.



var Test;
(function (Test) {
    var PromiseStatus;
    (function (PromiseStatus) {
        PromiseStatus["PENDING"] = "PENDING";
        PromiseStatus["RESOLVED"] = "RESOLVED";
        PromiseStatus["REJECTED"] = "REJECTED";
    })(PromiseStatus || (PromiseStatus = {}));
    ;
    var Promise = /** @class */ (function () {
        function Promise(setupFunc) {
            this._status = PromiseStatus.PENDING;
            //need to bind because _resolve and _reject will call by other object
            //for example: window when use setTimeout
            setupFunc(this._resolve.bind(this), this._reject.bind(this));
        }
        Promise.prototype.log = function (message) {
            console.log("[Promise] " + message);
        };
        Promise.prototype._resolve = function (result) {
            this._result = result;
            this.log(result.toString());
        };
        Promise.prototype._reject = function (reason) {
            this._reason = reason;
            this.log(reason.toString());
        };
        return Promise;
    }());
    Test.Promise = Promise;
})(Test || (Test = {}));
/// <reference path="Promise.ts" />
var PromiseInTs = Test.Promise;
var p = new PromiseInTs(function (resolve, reject) {
    setTimeout(function () {
        if (Math.random() > 0.5) {
            resolve('promise resolve');
        }
        else {
            reject('promise reject');
        }
    }, 100);
});
